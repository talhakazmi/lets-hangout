**Project Technical Details**

This project was built with YII v1.1.14 framework, This project includes two different parts one is API and other is frontend.
Both of them are created on Same YII v1.1.14 framework and work in a way that API provides RESTfull services to connect with frontend.
Both applications tests are written on YII framework test utility including automated tests and automated test perform automatic execution of application. --

## How To run project

First get git clone and install docker.

1. run Docker compose in root folder
2. get in to nginx bash and grant permission 777 to runtime, assets and images of API/code
3. get in to mysql image and run follwoing command to copy mysql dump
	sudo docker exec -i letshangout_mysql_1 mysql -uroot -proot letshangoutdb < ./lett_db.sql
4. not add letshangout.com in your hosts file to access application.
5. navigate to api.letshangout.com to add default data in to api server

---

## About project

This is an event planning application, you can perform following operations in it.

1. Add new plan
2. invite friend to the plan
3. add activities during plan execution for example dinner, movie and shopping
4. dashboard to review all plans
5. change plan and inform others.
6. invite guests to your plan.
7. allow suggestion from friends or guests.
8. track execution of plan step by step

