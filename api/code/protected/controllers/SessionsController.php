<?php

class SessionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
                        array(
                                'ext.starship.RestfullYii.filters.ERestFilter + 
                                REST.GET, REST.PUT, REST.POST, REST.DELETE'
                            ),
		);
	}
        
        /*
         * Add the ERestActionProvider to your controllers actions method.
         */
        public function actions()
        {
            return array(
                'REST.'=>'ext.starship.RestfullYii.actions.ERestActionProvider',
            );
        }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
                        array('allow', 
                                'actions'=>array('REST.GET', 'REST.PUT', 'REST.POST', 'REST.DELETE'),
                                'users'=>array('*'),
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Sessions;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sessions']))
		{
			$model->attributes=$_POST['Sessions'];
			
                        $model->userId  =   YII::app()->user->getId();
                        
                        if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sessions']))
		{
			$model->attributes=$_POST['Sessions'];
                        
                        $model->userId  =   YII::app()->user->getId();
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Sessions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Sessions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Sessions']))
			$model->attributes=$_GET['Sessions'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Sessions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Sessions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Sessions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sessions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function restEvents()
        {
            $model=new Sessions();
            
            /**
            * req.auth.username
            *
            * This is the username used to grant access to non-ajax users
            * At a minimum you should change this value
            *
            * @return (String) the username
            */ 
            $this->onRest('req.auth.username', function(){
                return 'admin@letshangout';
            });
            
            /**
            * req.auth.password
            *
            * This is the password use to grant access to non-ajax users
            * At a minimum you should change this value
            *
            * @return (String) the password
            */ 
            $this->onRest('req.auth.password', function(){
                return 'admin@Access';
            });
            
            /**
            * req.auth.user
            *
            * Used to validate a non-ajax user
            *
            * @param (String) (application_id) the application_id defined in config.application.id
            * @param (String) (username) the username defined in req.auth.username
            * @param (String) (password) the password defined in req.auth.password
            *
            * @return (Bool) true to grant access; false to deny access
            */ 
            $this->onRest('req.auth.user', function($application_id, $username, $password) {
                if(!isset($_SERVER['HTTP_X_'.$application_id.'_USERNAME']) || !isset($_SERVER['HTTP_X_'.$application_id.'_PASSWORD'])) {
                            return false;
                        }
                if( $username != $_SERVER['HTTP_X_'.$application_id.'_USERNAME'] ) {
                    return false;
                }
                if( $password != $_SERVER['HTTP_X_'.$application_id.'_PASSWORD'] ) {
                    return false;
                }
                return true;
            });
            
            /**
            * model.subresource.count
            *
            * Called when the count of sub-resources is needed
            *
            * @param (Object) (model) the model that represents the owner of the sub-resource
            * @param (String) (subresource_name) the name of the sub-resource
            * @param (Mixed/Int) (subresource_id) the primary key of the sub-resource
            *
            * @return (Int) count of the subresources
            */
            $this->onRest('model.sessions.override.attributes', function($model) {
                
                $total  =   0;
                $count  =   0;
                $percentage  =   0;
                if(!empty($model->rating))
                {
                    foreach($model->rating as $rating){
                        $total  +=   ($rating['rating']/10)*100;
                        $count++;
                    }
                    
                    $percentage =   ($total > 0)?number_format( $total/$count, 0 ):0;
                    
                }
                
                return array_merge($model->attributes, ['percent'=> $percentage. '%']);
            });
                
            $this->onRest('model.hidden.properties', function() {
                return ['user.code','user.password'];
            });
        }
}
