<?php

class ItemsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
                        array(
                                'ext.starship.RestfullYii.filters.ERestFilter + 
                                REST.GET, REST.PUT, REST.POST, REST.DELETE'
                            ),
		);
	}
        
        /*
         * Add the ERestActionProvider to your controllers actions method.
         */
        public function actions()
        {
            return array(
                'REST.'=>'ext.starship.RestfullYii.actions.ERestActionProvider',
            );
        }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','insertdata'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
                        array('allow', 
                                'actions'=>array('REST.GET', 'REST.PUT', 'REST.POST', 'REST.DELETE'),
                                'users'=>array('*'),
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Items;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Items']))
		{
			$model->attributes  =   $_POST['Items'];

                        $category   =   Itemcategories::model()->findByAttributes(array('id'=>$model->catId));
                        $pathtofile =   $this->imagePath($category->title);

                        $model->poster      =   CUploadedFile::getInstance($model,'poster');
                        $posterName         =   $this->imageUniqueName($model->poster->getType($model,'poster'));
                        
                        $model->banner      =   CUploadedFile::getInstance($model,'banner');
                        $bannerName         =   $this->imageUniqueName($model->banner->getType($model,'banner'));
                        
			if($model->save())

                            $model->poster->saveAs($pathtofile.$posterName);
                            $model->banner->saveAs($pathtofile.$bannerName);
                            $model->poster  =   $posterName;
                            $model->banner  =   $bannerName;

//                            $model->save();
                            $this->redirect(array('view','id'=>$model->id));
		}
                
                
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Items']))
		{
			$model->attributes=$_POST['Items'];
                        
                        $model->poster      =   CUploadedFile::getInstance($model,'poster');
                        $posterName         =   $this->imageUniqueName($model->poster->getType($model,'poster'));
                        
                        $model->banner      =   CUploadedFile::getInstance($model,'banner');
                        $bannerName         =   $this->imageUniqueName($model->banner->getType($model,'banner'));
                        
                        if($model->save())
                            $model->poster->saveAs($pathtofile.$posterName);
                            $model->banner->saveAs($pathtofile.$bannerName);
                            $model->poster  =   $posterName;
                            $model->banner  =   $bannerName;
                            $model->save();
                            $this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
                
                $category   =   Itemcategories::model()->findByPk($model->catId);;
                
                $poster   =   'images/'.strtolower($category->title).'/'.date('Y',strtotime($model->createdDate)).'/'.date('m',strtotime($model->createdDate)).'/'.date('d',strtotime($model->createdDate)).'/'.$model->poster;
                $banner   =   'images/'.strtolower($category->title).'/'.date('Y',strtotime($model->createdDate)).'/'.date('m',strtotime($model->createdDate)).'/'.date('d',strtotime($model->createdDate)).'/'.$model->banner;
                
                unlink($poster);
                unlink($banner);
                
                $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Items');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Items('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Items']))
			$model->attributes=$_GET['Items'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Items the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Items::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Items $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='items-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function restEvents()
        {
            $model=new Items();
            /**
            * req.auth.username
            *
            * This is the username used to grant access to non-ajax users
            * At a minimum you should change this value
            *
            * @return (String) the username
            */ 
            $this->onRest('req.auth.username', function(){
                return 'admin@letshangout';
            });
            
            /**
            * req.auth.password
            *
            * This is the password use to grant access to non-ajax users
            * At a minimum you should change this value
            *
            * @return (String) the password
            */ 
            $this->onRest('req.auth.password', function(){
                return 'admin@Access';
            });
            
            /**
            * req.auth.user
            *
            * Used to validate a non-ajax user
            *
            * @param (String) (application_id) the application_id defined in config.application.id
            * @param (String) (username) the username defined in req.auth.username
            * @param (String) (password) the password defined in req.auth.password
            *
            * @return (Bool) true to grant access; false to deny access
            */ 
            $this->onRest('req.auth.user', function($application_id, $username, $password) {
                if(!isset($_SERVER['HTTP_X_'.$application_id.'_USERNAME']) || !isset($_SERVER['HTTP_X_'.$application_id.'_PASSWORD'])) {
                            return false;
                        }
                if( $username != $_SERVER['HTTP_X_'.$application_id.'_USERNAME'] ) {
                    return false;
                }
                if( $password != $_SERVER['HTTP_X_'.$application_id.'_PASSWORD'] ) {
                    return false;
                }
                return true;
            });
            
            /**
            * model.subresource.count
            *
            * Called when the count of sub-resources is needed
            *
            * @param (Object) (model) the model that represents the owner of the sub-resource
            * @param (String) (subresource_name) the name of the sub-resource
            * @param (Mixed/Int) (subresource_id) the primary key of the sub-resource
            *
            * @return (Int) count of the subresources
            */
            $this->onRest('model.items.override.attributes', function($model) {
                
                $total  =   0;
                $count  =   0;
                $percentage  =   0;
                if(!empty($model->rating))
                {
                    foreach($model->rating as $rating){
                        $total  +=   ($rating['rating']/10)*100;
                        $count++;
                    }
                    
                    $percentage =   ($total > 0)?number_format( $total/$count, 0 ):0;
                    
                }
                return array_merge($model->attributes, ['percent'=> $percentage. '%']);
            });
            
            
            $this->onRest('req.get.autocompleteitems.render', function() {
                
                $items=  Items::model()->with(array(
                                'sessions'=>array(
                                    'select'=>'stateRoomId',
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'sessions.status=\''.$_GET['status'].'\'',
                                ),
                                'cat'=>array(
                                    'select'=>false,
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'cat.title=\''.$_GET['cat'].'\'',
                                ),
                            ))->findAll(array('condition'=>'t.title like \'%'.$_GET['term'].'%\''));
                
                echo CJSON::encode($items);
            });


            $this->onRest('req.get.fetchitems.render', function() {
                
                $items=  Items::model()->with(array(
                                'sessions'=>array(
                                    'select'=>'stateRoomId',
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'sessions.status=\''.$_GET['status'].'\'',
                                ),
                                'cat'=>array(
                                    'select'=>false,
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'cat.title=\''.$_GET['cat'].'\'',
                                ),
                                'venue'=>array(
                                    'select'=>false,
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'venue.city=\''.$_GET['city'].'\'',
                                ),
                            ))->findAll();
                
                echo CJSON::encode($items);
            });
            
            $this->onRest('req.get.autocompletesessions.render', function() {
                
                $sessions= Sessions::model()->with(array('venue'=>array('joinType'=>'INNER JOIN')))->findAll(array('condition'=>'itemId='.$_GET['itemid']));
               
                $data = array();
                $count=0;
               /* foreach($sessions as $session)
                {
                    $data[$session['venue']['title']][$count]['session']['id'] =   $session['id'];
                    $data[$session['venue']['title']][$count]['session']['startTime'] =   $session['startTime'];
                    $count++;
                }*/
                
               foreach($sessions as $key => $venue)
                {   
                    
                    $newdate = strtotime($venue['startTime']);
                    $date = date('Y-m-d', $newdate);
                    $tempData[$venue['venue']['id']]['venue']['title'] = $venue['venue']['title'];
                    $tempData[$venue['venue']['id']]['venue']['sessions'][$date][] = array("id" => $venue['id'] ,"startTime" => $venue['startTime']);
                    //$tempData[$venue['venue']['id']]['venue']['sessions'][$key]['startTime'] = $venue['startTime'];    
                    //$data[$venue['venue']['id']] = $venue['venue']['title'];                     
                    
                    $count++;                    
                }

               
                //change tempData array key into ordered number
                foreach($tempData as $value){
                    $data[] = $value;
                }
                        
                echo CJSON::encode($data);
            });


            //Created By: Waqas Karim
            //Date: 3/4/2015
            //Purpose: Get all sessions and their venues for Mobile 
             $this->onRest('req.get.getitems.render', function() {
                
                $items=  Items::model()->with(array(
                                'sessions'=>array(
                                    'select'=>'stateRoomId',
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'sessions.status=\''.$_GET['status'].'\'',
                                ),
                                'cat'=>array(
                                    'select'=>false,
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'cat.title=\''.$_GET['cat'].'\'',
                                ),
                                'venue'=>array(
                                    'select'=>false,
                                    // but want to get only users with published posts
                                    'joinType'=>'INNER JOIN',
                                    'condition'=>'venue.city=\''.$_GET['city'].'\'',
                                ),
                            ))->findAll();                
                    
                $data = array();
                foreach($items as $key => $item){
                    $data['items'][$key]['id'] = $item['id'];
                    $data['items'][$key]['title'] = $item['title'];
                    $data['items'][$key]['banner'] = $this->convertImageUrl($item['poster'], $item['createdDate'], $_GET['cat']);

                    //get item's venues and session                                     
                    $sessions= Sessions::model()->with(array('venue'=>array('joinType'=>'INNER JOIN')))->findAll(array('condition' => "itemId=".$item['id']." AND t.status='available'"));                    
                    foreach($sessions as $k => $venue)
                    {   
                        //$data['items'][$key]['venue'][$k][$venue['venue']['title']] = $venue['venue']['title'];
                        //create new date
                        $newdate = strtotime($venue['startTime']);
                        $date = date('Y-m-d', $newdate);
                        $data['items'][$key]['venue'][$venue['venue']['title']]['sessions'][$date][] = array("id" => $venue['id'] ,"startTime" => $venue['startTime']);                                   
                    }
                    
                } 
                echo CJSON::encode($data);
            });
            
        }
        
        public function actionInsertData()
	{
        $postData   =   CJSON::decode(file_get_contents('php://input'));

	    if(isset($postData['file']) && !empty($postData['file']))
        {
            $category   =   Itemcategories::model()->findByAttributes(array('id'=>$postData['catId']));
            $pathtofile =   $this->imagePath($category->title);
            $posterName =   $this->imageUniqueName($postData['fileType']);
            copy($postData['file'], $pathtofile.$posterName);
        }

        $command = Yii::app()->db->createCommand();
        // INSERT INTO `items` (`title`, `catId`, `poster`) VALUES (:name, :email, :posternames)
        $command->insert('items', array(
            'title'=>$postData['title'],
            'catId'=>$postData['catId'],
            'description'=>(isset($postData['description'])?$postData['description']:''),
            'poster'=>(isset($posterName)?$posterName:''),
        ));

        echo CJSON::encode(array('id'=>Yii::app()->db->lastInsertID));
	}


    //This method create item poster url
    //Required params: Image Name, Item CreatedDate and Item Category
    private function convertImageUrl($src, $date, $category){
        $newDate = date_parse($date);
        $parseDateToUrl = Yii::app()->getBaseUrl(true) . '/images/' . $category . '/' . $newDate['year'] . '/' . $newDate['month'] . '/' . $newDate['day'] . '/' . $src;
        return $parseDateToUrl;        
    }


}
