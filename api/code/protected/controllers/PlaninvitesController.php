<?php

class PlaninvitesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
						array(
                                'ext.starship.RestfullYii.filters.ERestFilter + 
                                REST.GET, REST.PUT, REST.POST, REST.DELETE'
                            ),
		);
	}
	
	/*
	 * Add the ERestActionProvider to your controllers actions method.
	 */
	public function actions()
	{
		return array(
			'REST.'=>'ext.starship.RestfullYii.actions.ERestActionProvider',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('allow', 
				'actions'=>array('REST.GET', 'REST.PUT', 'REST.POST', 'REST.DELETE'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Planinvites;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Planinvites']))
		{
			$model->attributes=$_POST['Planinvites'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Planinvites']))
		{
			$model->attributes=$_POST['Planinvites'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Planinvites');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Planinvites('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Planinvites']))
			$model->attributes=$_GET['Planinvites'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Planinvites the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Planinvites::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Planinvites $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='planinvites-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function restEvents()
    {
		/**
		* req.auth.username
		*
		* This is the username used to grant access to non-ajax users
		* At a minimum you should change this value
		*
		* @return (String) the username
		*/ 
		$this->onRest('req.auth.username', function(){
			return 'admin@letshangout';
		});
		
		/**
		* req.auth.password
		*
		* This is the password use to grant access to non-ajax users
		* At a minimum you should change this value
		*
		* @return (String) the password
		*/ 
		$this->onRest('req.auth.password', function(){
			return 'admin@Access';
		});
		
		/**
		* req.auth.user
		*
		* Used to validate a non-ajax user
		*
		* @param (String) (application_id) the application_id defined in config.application.id
		* @param (String) (username) the username defined in req.auth.username
		* @param (String) (password) the password defined in req.auth.password
		*
		* @return (Bool) true to grant access; false to deny access
		*/ 
		$this->onRest('req.auth.user', function($application_id, $username, $password) {
			if(!isset($_SERVER['HTTP_X_'.$application_id.'_USERNAME']) || !isset($_SERVER['HTTP_X_'.$application_id.'_PASSWORD'])) {
						return false;
					}
			if( $username != $_SERVER['HTTP_X_'.$application_id.'_USERNAME'] ) {
				return false;
			}
			if( $password != $_SERVER['HTTP_X_'.$application_id.'_PASSWORD'] ) {
				return false;
			}
			return true;
		});
		
                $this->onRest('req.post.friends.render', function($data) {
                    $emails =   explode(',', $data['emails']);

                    $planOwner  = User::model()->findByPk($data['userId']);

                    foreach($emails as $email)
                    {
                        //search for user existence
                        $user   = User::model()->findByAttributes(array("email"=>$email));

                        if($user)
                        {
                            //search for friend relation existence
                            $friend   = Friends::model()->findByAttributes(array("friendId"=>$user->id,"userId"=>$data['userId']));

                            if($friend)
                            {
                                $planInvites    = Planinvites::model()->findByAttributes(array("friendsId"=>$friend->id,"planId"=>$data['planId']));

                                if($planInvites)
                                {
                                    //Add notification
                                    $notifications = new Notifications;
                                    $notifications->userId = $user->id;
                                    $notifications->senderId =$data['userId'];
                                    $notifications->code = CJSON::encode(array('link'=>Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'],'label'=>ucfirst($planOwner->name) . ' invited you in his Plan'));
                                    $notifications->save();
                                    
                                    $this->sendEmail($email,'Invitation to the plan',array('name'=>$user->name, 'message' => 'You are invited in the plan by '.ucfirst($planOwner->name), 'link' => Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'], 'label'=>'View plan detail', 'sender' => ucfirst($planOwner->name)));
                                    return array("success"=>true,"message"=>"Friend invited");
                                }
                                else 
                                {
                                    //Add planInvites
                                    $planInvites = new Planinvites;
                                    $planInvites->planId=$data['planId'];
                                    $planInvites->friendsId=$friend->id;

                                    if($planInvites->save())
                                    {
                                        //Add notification
                                        $notifications = new Notifications;
                                        $notifications->userId = $user->id;
                                        $notifications->senderId =$data['userId'];
                                        $notifications->code = CJSON::encode(array('link'=>Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'],'label'=>ucfirst($planOwner->name) . ' invited you in his Plan'));
                                        $notifications->save();
                                        
                                        $this->sendEmail($email,'Invitation to the plan',array('name'=>$user->name, 'message' => 'You are invited in the plan by '.ucfirst($planOwner->name), 'link' => Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'], 'label'=>'View plan detail', 'sender' => ucfirst($planOwner->name)));
                                        return array("success"=>true,"message"=>"Friend invited");
                                    }
                                    else
                                    {
                                        return array("success"=>false,"message"=>"System is unable to invite friend");
                                    }
                                }
                            }
                            else
                            {
                                //Add friend request
                                $friend = new Friends;
                                $friend->userId      =   $data['userId'];
                                $friend->friendId    =   $user->id;    
                                $friend->status      =   'pending';

                                if($friend->save())
                                {
                                    //Add planInvites
                                    $planInvites = new Planinvites;
                                    $planInvites->planId=$data['planId'];
                                    $planInvites->friendsId=$friend->id;

                                    if($planInvites->save())
                                    {
                                        //Add notification
                                        $notifications = new Notifications;
                                        $notifications->userId = $user->id;
                                        $notifications->senderId =$data['userId'];
                                        $notifications->code = CJSON::encode(array('link'=>Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'],'label'=>ucfirst($planOwner->name) . ' invited you in his Plan'));
                                        $notifications->save();
                                        
                                        //Create a new freind relation with user
                                        $this->sendEmail($email,'Invitation to the plan',array('name'=>$user->name, 'message' => 'You are invited in the plan by '.ucfirst($planOwner->name), 'link' => Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'], 'label'=>'View plan detail', 'sender' => ucfirst($planOwner->name)));
                                        return array("success"=>true,"message"=>"Friend invited");
                                    }
                                    else
                                    {
                                        return array("success"=>false,"message"=>"System is unable to invite friend");
                                    }
                                }
                                else
                                {
                                    return array("success"=>false,"message"=>"System is unable to add friend");
                                }
                            }
                        }  
                        else
                        {       
                            //Generate random password
                            $password   =   mt_rand(5, 15);
                            //Create a new user
                            $user   = new User;
                            $user->email = $email;
                            $user->name = 'Guest';
                            $user->code = md5($email);
                            $user->isActive = 0;
                            $user->password = md5($password);
                            $user->repeat_password = md5($password);

                            if($user->save())
                            {
                                //Add friend request
                                $friend = new Friends;
                                $friend->userId      =   $data['userId'];
                                $friend->friendId    =   $user->id;    
                                $friend->status      =   'pending';

                                if($friend->save())
                                {
                                    //Add planInvites
                                    $planInvites = new Planinvites;
                                    $planInvites->planId=$data['planId'];
                                    $planInvites->friendsId=$friend->id;

                                    if($planInvites->save())
                                    {
                                        //Add notification
                                        $notifications = new Notifications;
                                        $notifications->userId = $user->id;
                                        $notifications->senderId =$data['userId'];
                                        $notifications->code = CJSON::encode(array('link'=>Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'],'label'=>ucfirst($planOwner->name) . ' invited you in his Plan'));
                                        $notifications->save();
                                        
                                        //Create a new freind relation with user
                                        $this->sendEmail($email,'Invitation to the plan',array('name'=>$user->name, 'message' => 'You are invited in the plan by '.ucfirst($planOwner->name), 'link' => Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$data['planId'], 'label'=>'View plan detail', 'sender' => ucfirst($planOwner->name)));
                                        return array("success"=>true,"message"=>"Friend invited");
                                    }
                                    else
                                    {
                                        return array("success"=>false,"message"=>"System is unable to invite friend");
                                    }
                                }
                                else
                                {
                                    return array("success"=>false,"message"=>"System is unable to add friend");
                                }
                            }
                            else
                            {
                                return array("success"=>false,"message"=>"System is unable to invite friend");
                            }
                        }
                    }
		});
	}
}
