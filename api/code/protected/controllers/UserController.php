<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
                        array(
                            'ext.starship.RestfullYii.filters.ERestFilter + 
                            REST.GET, REST.PUT, REST.POST, REST.DELETE'
                        ),
		);
	}

        /*
         * Add the ERestActionProvider to your controllers actions method.
         */
        public function actions()
        {
            return array(
                'REST.'=>'ext.starship.RestfullYii.actions.ERestActionProvider',
            );
        }
        
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
            array('allow',
                    'actions'=>array('REST.GET', 'REST.PUT', 'REST.POST', 'REST.DELETE'),
                    'users'=>array('*'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function restEvents()
        {
            /**
            * req.auth.username
            *
            * This is the username used to grant access to non-ajax users
            * At a minimum you should change this value
            *
            * @return (String) the username
            */ 
            $this->onRest('req.auth.username', function(){
                return 'admin@letshangout';
            });
            
            /**
            * req.auth.password
            *
            * This is the password use to grant access to non-ajax users
            * At a minimum you should change this value
            *
            * @return (String) the password
            */ 
            $this->onRest('req.auth.password', function(){
                return 'admin@Access';
            });
            
            /**
            * req.auth.user
            *
            * Used to validate a non-ajax user
            *
            * @param (String) (application_id) the application_id defined in config.application.id
            * @param (String) (username) the username defined in req.auth.username
            * @param (String) (password) the password defined in req.auth.password
            *
            * @return (Bool) true to grant access; false to deny access
            */ 
            
            $this->onRest('req.auth.user', function($application_id, $username, $password) {
                if(!isset($_SERVER['HTTP_X_'.$application_id.'_USERNAME']) || !isset($_SERVER['HTTP_X_'.$application_id.'_PASSWORD'])) {
                            return false;
                        }
                if( $username != $_SERVER['HTTP_X_'.$application_id.'_USERNAME'] ) {
                    return false;
                }
                if( $password != $_SERVER['HTTP_X_'.$application_id.'_PASSWORD'] ) {
                    return false;
                }
                return true;
            });
            
            $this->onRest('post.filter.req.post.resource.render', function($json) {
                $user   =   CJSON::decode($json);
                if($user['data']['user']['name'] != 'Guest')
                {
                    $this->sendEmail($user['data']['user']['email'],'Welcome to LetsHangout',array('name'=>$user['data']['user']['name'], 'message' => 'Welcome to Let`s Hanout, the one place to create Plans.', 'link' => Yii::app()->params['frontEndServer'].'user/activateuser/code/'.$user['data']['user']['code'], 'label'=>'Verify email', 'sender'=>'Team Let`s Hangout'));
                }
                return $json;
            });
            
            $this->onRest('req.post.sendemail.render', function($data) {
                
                $this->sendEmail($data['email'],$data['subject'],array('name'=>$data['name'], 'message' => $data['message'], 'link' => $data['link'], 'label'=>$data['label'], 'sender' => $data['sender']));
                
                return true;
            });
            
            $this->onRest('req.get.plans.render', function($id) {
                
                $planModel   =   new Plan();
                $plans  =   $planModel->with(array(
                                                'planinvites',
                                                'planinvites.friends'=>array(
                                                                'alias'=>'user',
                                                                'condition'=>'(user.friendId='.$id.') OR (p.userId='.$id.')'
                                                            ),
                                                ))->findAll(array('alias'=>'p', 'order'=>'p.status, p.createdDate ASC'));
                
                if(!empty($plans))
                {
                    foreach ($plans as $plan)
                    {
                        $yes=0;$no=0;$maybe=0;
                        foreach($plan['planinvites'] as $invites)
                        {
                            switch ($invites['initialStatus'])
                            {
                                case 'yes':
                                    $yes++;
                                    break;
                                case 'no':
                                    $no++;
                                    break;
                                case 'maybe':
                                    $maybe++;
                                    break;
                            }
                        }
                        $data[]   =   array_merge($plan->attributes, ['invitations'=> array('yes'=>$yes,'maybe'=>$maybe,'no'=>$no)]);
                    }
                    return $data;
                }
                else
                {
                    throw new CHttpException(404,'The specified post cannot be found.');
                }
            });
            
            /*$this->onRest('model.hidden.properties', function() {
                return ['password'];
            });*/
        }
}
