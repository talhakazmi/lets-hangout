<?php

class PlanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
						array(
                                'ext.starship.RestfullYii.filters.ERestFilter + 
                                REST.GET, REST.PUT, REST.POST, REST.DELETE'
                            ),
		);
	}
	
	/*
	 * Add the ERestActionProvider to your controllers actions method.
	 */
	public function actions()
	{
		return array(
			'REST.'=>'ext.starship.RestfullYii.actions.ERestActionProvider',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('allow', 
				'actions'=>array('REST.GET', 'REST.PUT', 'REST.POST', 'REST.DELETE'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Plan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Plan']))
		{
			$model->attributes=$_POST['Plan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Plan']))
		{
			$model->attributes=$_POST['Plan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Plan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Plan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Plan']))
			$model->attributes=$_GET['Plan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Plan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Plan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Plan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='plan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function restEvents()
        {
                $model=new Plan;
		/**
		* req.auth.username
		*
		* This is the username used to grant access to non-ajax users
		* At a minimum you should change this value
		*
		* @return (String) the username
		*/ 
		$this->onRest('req.auth.username', function(){
			return 'admin@letshangout';
		});
		
		/**
		* req.auth.password
		*
		* This is the password use to grant access to non-ajax users
		* At a minimum you should change this value
		*
		* @return (String) the password
		*/ 
		$this->onRest('req.auth.password', function(){
			return 'admin@Access';
		});
		
		/**
		* req.auth.user
		*
		* Used to validate a non-ajax user
		*
		* @param (String) (application_id) the application_id defined in config.application.id
		* @param (String) (username) the username defined in req.auth.username
		* @param (String) (password) the password defined in req.auth.password
		*
		* @return (Bool) true to grant access; false to deny access
		*/ 
		$this->onRest('req.auth.user', function($application_id, $username, $password) {
			if(!isset($_SERVER['HTTP_X_'.$application_id.'_USERNAME']) || !isset($_SERVER['HTTP_X_'.$application_id.'_PASSWORD'])) {
						return false;
					}
			if( $username != $_SERVER['HTTP_X_'.$application_id.'_USERNAME'] ) {
				return false;
			}
			if( $password != $_SERVER['HTTP_X_'.$application_id.'_PASSWORD'] ) {
				return false;
			}
			return true;
		});
		
                /**
            * model.subresource.count
            *
            * Called when the count of sub-resources is needed
            *
            * @param (Object) (model) the model that represents the owner of the sub-resource
            * @param (String) (subresource_name) the name of the sub-resource
            * @param (Mixed/Int) (subresource_id) the primary key of the sub-resource
            *
            * @return (Int) count of the subresources
            */
            $this->onRest('model.plan.override.attributes', function($model) {
                
                    $yes=0;$no=0;$maybe=0;
                if(!empty($model->planinvites))
                {
                    foreach($model->planinvites as $invite){
                        
                        switch($invite['initialStatus']){
                            case 'yes':
                                $yes++;
                                break;
                            case 'no':
                                $no++;
                                break;
                            case 'maybe':
                                $maybe++;
                                break;
                        }
                        
                        $friend=Friends::model()->findByPk($invite['friendsId']);
                        $user=User::model()->findByPk($friend['friendId']);
                        $data[$invite['friendsId']]   =   array('name'=> $user['name'],'email'=> $user['email'],'userId'=>$user['id']);
                    }
                    
                }
                else
                {
                    $data = array();
                }
                
                return array_merge($model->attributes,['invited'=>$data, 'initialStatus'=>array('yes'=>$yes,'no'=>$no,'maybe'=>$maybe)]);
                
            });
                
            $this->onRest('model.hidden.properties', function() {
                return ['user.password','user.code'];
            });
            
            $this->onRest('req.get.detail.render', function($id) {
                
                $planModel   =   new Plan();
                $plans  =   $planModel->with(array(
                                                'planinvites',
                                                'planinvites.user'=>array(
                                                                'alias'=>'u',
                                                                'select'=>'u.name'
                                                            ),
                                                'plansuggestions'=>array(
                                                                'alias'=>'ps',
                                                                'select'=>'ps.id'
                                                            ),
                                                'plansuggestions.plansuggestionvotes'=>array(
                                                                'alias'=>'vote',
                                                                'select'=>'vote.id,vote.vote,vote.userId'
                                                            ),
                                                'plansuggestions.sessions'=>array(
                                                                'alias'=>'s',
                                                                'select'=>'s.startTime'
                                                            ),
                                                'plansuggestions.sessions.item'=>array(
                                                                'alias'=>'i',
                                                                'select'=>'i.title'
                                                            ),
                                                'plansuggestions.sessions.item.cat'=>array(
                                                                'alias'=>'c',
                                                                'select'=>'c.title'
                                                            ),
                                                'plansuggestions.sessions.venue'=>array(
                                                                'alias'=>'v',
                                                                'select'=>'v.title,v.address',
                                                            ),
                                                )
                                            )->findAll(array('alias'=>'p', 'condition'=>'p.id='.$id, 'order'=>'p.status, p.createdDate ASC'));
                if(!empty($plans))
                {
                    foreach ($plans as $plan)
                    {
                        $yes=0;$no=0;$maybe=0;
                        $planInvitees   =   array();
                        foreach($plan['planinvites'] as $invite)
                        {
                            switch ($invite['initialStatus'])
                            {
                                case 'yes':
                                    $yes++;
                                    break;
                                case 'no':
                                    $no++;
                                    break;
                                case 'maybe':
                                    $maybe++;
                                    break;
                            }
                            $Invitees['id']   =   $invite['id'];
                            $Invitees['initialStatus']   =   $invite['initialStatus'];
                            $Invitees['finalStatus']   =   $invite['finalStatus'];
                            $Invitees['userId']   =   $invite['user']['id'];
                            $Invitees['name']   =   $invite['user']['name'];
                            $planInvitees[] =   $Invitees;
                        }

                        foreach($plan['plansuggestions'] as $suggestions)
                        {
                            $suggestion[] =   array(
                                                'id'=>$suggestions['id'],
                                                'votes'=>$suggestions['plansuggestionvotes'],
                                                'startTime'=>$suggestions['sessions']['startTime'],
                                                'title'=>$suggestions['sessions']['item']['title'],
                                                'category'=>$suggestions['sessions']['item']['cat']['title'],
                                                'venue'=>$suggestions['sessions']['venue']['title'],
                                                'venueAddress'=>$suggestions['sessions']['venue']['address'],
                                            );
                        }
                        $data   =   array_merge($plan->attributes, ['suggestions'=> $suggestion,'invitations'=> array('yes'=>$yes,'maybe'=>$maybe,'no'=>$no),'planInvitee'=>$planInvitees]);
                    }
                    return $data;
                }
                else
                {
                    throw new CHttpException(404,'The specified post cannot be found.');
                }
            });
            
            $this->onRest('req.post.create.render', function($data) {
                
                if (0 < $data['sessionId'])
                {
                    $sessionId = $data['sessionId'];
                }
                else
                {
                    $itemCat = $data['itemCategory'];
                    $venueCat = 0;

                    switch ($itemCat)
                    {
                        case 'coffee':
                            $venueCat = 4; //restaurant
                            $itemCat = 10; //coffee
                            break;
                        case 'movies':
                            $venueCat = 1; //Cinema
                            $itemCat = 4; //Movie
                            break;
                        case 'event':
                            $venueCat = 7; //Custom
                            $itemCat = 6; //event
                            break;
                        case 'lunch':
                        case 'dinner':
                            $venueCat = 4; //restaurant
                            $itemCat = 5; //food
                            break;
                        case 'other':
                            $venueCat = 7; //custom
                            $itemCat = 9; //custom
                            break;
                    }
                    
                    //Add Custom Venue
                    $venue = new Venue;
                    $venue->title=$data['venue'];
                    $venue->address =$data['venue'];
                    $venue->city =$data['city'];
                    $venue->country =$data['country'];
                    $venue->catId =$venueCat;
                    $venue->userId =$data['userId'];
                    $venue->save();

                    //Add StateRoom
                    $stateRoom = new Stateroom;
                    $stateRoom->title='Room';
                    $stateRoom->venueId=$venue->id;
                    $stateRoom->save();
                    
                    //Add Item
                    $items = new Items;
                    $items->title=$data['title'];
                    $items->description=(isset($data['description']) ? $data['description'] : '');
                    $items->catId=$itemCat;
                    $items->save();
                    
                    //Add Session
                    $sessions = new Sessions;
                    $sessions->itemId=$items->id;
                    $sessions->stateRoomId=$stateRoom->id;
                    $sessions->startTime=date('Y-m-d', strtotime($data['startDate'])) . ' ' . date('h:i:s', strtotime($data['startDate']));;
                    $sessions->description=(isset($data['description']) ? $data['description'] : '');
                    $sessions->userId=$data['userId'];
                    $sessions->save();
                    
                    $sessionId = $sessions->id;
                }
                
                if(!isset($sessionId) || empty($sessionId))
                    return array("success"=>false,"message"=>"Session is not found");
                
                if(!isset($data['planId']))
                {
                    
                    $sessionDetail  =   Sessions::model()->with(array('item'=>array('alias'=>'i','select'=>'i.title')))->findByPk($sessionId);
                            
                    $isActive = 1;
                    //Create a new plan
                    $plan   =   new Plan;
                    $plan->userId = $data['userId'];
                    $plan->title = $data['title'];
                    $plan->isSuggestion = $data['isSuggestion'];
                    $plan->isInvite = $data['isInvite'];
                    $plan->save();

                    $planId = $plan->id;
                }
                else
                {
                    $isActive = 0;
                    $planId = $data['planId'];
                    $planData=new Plan();
                    $plan=$planData->findByPk($planId);
                }
                //Create a new plan suggestion
                $Plansuggestions=new Plansuggestions;
                $Plansuggestions->userId=$data['userId'];
                $Plansuggestions->planId=$planId;
                $Plansuggestions->sessionsId=$sessionId;
                $Plansuggestions->isActive=$isActive;
                $Plansuggestions->save();
                
                if($isActive == 0)
                {
                    $planInvites    =   new Planinvites();
                    $invites    =   $planInvites->with(array('friends.friend'=>array('alias'=>'f','select'=>'f.email')))->findAll(array('alias'=>'i', 'condition'=>'i.planId='.$planId));

                    foreach($invites as $invitees)
                    {
                        if($data['userId'] != $invitees['friends']['friend']['id'])
                        {

                            $user   =   new User();
                            $userData   =   $user->findByPk($data['userId']);
                            //Create a new notification
                            $notifications = new Notifications;
                            $notifications->userId = $invitees['friends']['friend']['id'];
                            $notifications->senderId =$data['userId'];
                            $notifications->code = CJSON::encode(array('link'=>Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$planId,'label'=>$userData->name . ' suggest in  '.$data['title']));
                            $notifications->save();
                        }
                    }
                    if($data['userId'] != $plan->userId)
                    {
                        $user   =   new User();
                        $userData   =   $user->findByPk($data['userId']);
                        //Create a new notification
                        $notifications = new Notifications;
                        $notifications->userId = $invitees['friends']['friend']['id'];
                        $notifications->senderId =$data['userId'];
                        $notifications->code = CJSON::encode(array('link'=>Yii::app()->params['frontEndServer'].'/plan/detail/id/'.$planId,'label'=>$userData->name . ' suggest in  '.$data['title']));
                        $notifications->save();
                    }
                }
                return array("success"=>true,"message"=>"Record(s) inserted","data"=>array("plan"=>array("id"=>$planId)));
            });
	}
}
