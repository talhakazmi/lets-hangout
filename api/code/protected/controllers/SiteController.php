<?php

class SiteController extends Controller
{
	public function init()
        {
                $this->menu = array(
                        array('label'=>'Venue', 'url'=>array('/venue')),
                        array('label'=>'Venue Categories', 'url'=>array('/venuecategories')),
                        array('label'=>'Items', 'url'=>array('/items')),
                        array('label'=>'Item Categories', 'url'=>array('/itemcategories')),
                        array('label'=>'State Room', 'url'=>array('/stateroom')),
                        array('label'=>'Sessions', 'url'=>array('/sessions')),
                        array('label'=>'Reservations', 'url'=>array('/reservation')),
                        array('label'=>'Users', 'url'=>array('/user')),
                        array('label'=>'Booking Agent', 'url'=>array('/bookingagent')),
                        array('label'=>'Comments', 'url'=>array('/comments')),
                        array('label'=>'Rating', 'url'=>array('/rating')),
                        array('label'=>'Early Access List', 'url'=>array('/earlyAccessList')),
                        array('label'=>'Friends Circles', 'url'=>array('/circle')),
                        array('label'=>'Friends', 'url'=>array('/friends')),
                        array('label'=>'Friends to circle', 'url'=>array('/friendscircle')),
                        array('label'=>'Plans', 'url'=>array('/plan')),
                );
                parent::init();
        }
    
        /**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}