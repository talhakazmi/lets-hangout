<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
        
        public function imagePath($type)
        {
            $year   =   date('Y');
            $month   =   date('m');
            $day   =   date('d');
            
            if (!is_dir('images/'.$type)) {
                mkdir('images/'.$type);         
            }
            if (!is_dir('images/'.$type.'/'.$year)) {
                mkdir('images/'.$type.'/'.$year);         
            }
            if (!is_dir('images/'.$type.'/'.$year.'/'.$month)) {
                mkdir('images/'.$type.'/'.$year.'/'.$month);         
            }
            if (!is_dir('images/'.$type.'/'.$year.'/'.$month.'/'.$day)) {
                mkdir('images/'.$type.'/'.$year.'/'.$month.'/'.$day);         
            }
            
            return 'images/'.$type.'/'.$year.'/'.$month.'/'.$day.'/';
        }
        
        public function imageUniqueName($mimeType)
        {
            switch($mimeType)
            {
                case 'image/gif':
                    $name   =   uniqid() . '.gif';
                    break;
                case 'image/png':
                    $name   =   uniqid() . '.png';
                    break;
                case 'image/jpg':
                    $name   =   uniqid() . '.jpg';
                    break;
                case 'image/jpeg':
                    $name   =   uniqid() . '.jpeg';
                    break;
                case 'image/pjpeg':
                    $name   =   uniqid() . '.jpeg';
                    break;
            }
            return $name;
        }
        
        public function sendEmail($to,$subject,$data){
            $mail = new YiiMailer('contact', $data);
            $mail->setFrom('blimp.talha@gmail.com', 'John Doe');
            
            $mail->setTo($to);
            $mail->setSubject($subject);

            $mail->setSmtp('smtp.gmail.com', 465, 'ssl', true, 'blimp.talha@gmail.com', 'blimp123');

            if ($mail->send()) {
                return true;
            } else {
                return false;
            }
        }
        
}