<?php

/**
 * This is the model class for table "reservation".
 *
 * The followings are the available columns in table 'reservation':
 * @property string $id
 * @property string $sessionId
 * @property integer $seatsReserved
 * @property string $seatsPosition
 * @property integer $cost
 * @property string $status
 * @property string $bookingAgentId
 * @property string $createdDate
 * @property string $userId
 *
 * The followings are the available model relations:
 * @property Sessions $session
 * @property Bookingagent $bookingAgent
 * @property User $user
 */
class Reservation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reservation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sessionId, seatsReserved, seatsPosition, cost, bookingAgentId, userId', 'required'),
			array('seatsReserved, cost', 'numerical', 'integerOnly'=>true),
			array('sessionId, bookingAgentId, userId', 'length', 'max'=>10),
			array('seatsPosition', 'length', 'max'=>45),
			array('status', 'length', 'max'=>9),
			array('createdDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sessionId, seatsReserved, seatsPosition, cost, status, bookingAgentId, createdDate, userId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'session' => array(self::BELONGS_TO, 'Sessions', 'sessionId'),
			'bookingAgent' => array(self::BELONGS_TO, 'Bookingagent', 'bookingAgentId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sessionId' => 'Session',
			'seatsReserved' => 'Seats Reserved',
			'seatsPosition' => 'Seats Position',
			'cost' => 'Cost',
			'status' => 'Status',
			'bookingAgentId' => 'Booking Agent',
			'createdDate' => 'Created Date',
			'userId' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sessionId',$this->sessionId,true);
		$criteria->compare('seatsReserved',$this->seatsReserved);
		$criteria->compare('seatsPosition',$this->seatsPosition,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('bookingAgentId',$this->bookingAgentId,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('userId',$this->userId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reservation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
