<?php

/**
 * This is the model class for table "items".
 *
 * The followings are the available columns in table 'items':
 * @property string $id
 * @property string $title
 * @property string $poster
 * @property string $banner
 * @property string $catId
 * @property string $description
 * @property string $trailer
 * @property string $createdDate
 *
 * The followings are the available model relations:
 * @property Itemcategories $cat
 * @property Sessions[] $sessions
 */
class Items extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, catId', 'required'),
			array('title, trailer', 'length', 'max'=>45),
			array('catId', 'length', 'max'=>10),
			//array('title', 'safe'),
                        array('poster', 'file', 'allowEmpty' => TRUE, 'types'=>'jpg, gif, png'),
                        array('banner', 'file', 'allowEmpty' => TRUE, 'types'=>'jpg, gif, png'),
                        array('createdDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, poster, banner, catId, description, trailer, createdDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cat' => array(self::BELONGS_TO, 'Itemcategories', 'catId'),
			'venueCat'=>array(self::BELONGS_TO, 'Venuecategories',array('venueCatId'=>'id'), 'through'=>'cat'),
			'sessions' => array(self::HAS_MANY, 'Sessions', 'itemId', 'order'=>'startTime ASC'),
			'stateRoom' => array(self::BELONGS_TO, 'Stateroom', array('stateRoomId'=>'id'), 'through'=>'sessions'),
			'venue'=>array(self::BELONGS_TO, 'Venue', array('venueId'=>'id'), 'through'=>'stateRoom'),
			'rating' => array(self::HAS_MANY, 'Rating', 'postId', 'condition'=>'rating.type=\'item\''),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'poster' => 'Poster',
			'banner' => 'Banner',
			'catId' => 'Cat',
			'description' => 'Description',
			'trailer' => 'Trailer',
			'createdDate' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('poster',$this->poster,true);
		$criteria->compare('banner',$this->banner,true);
		$criteria->compare('catId',$this->catId,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('trailer',$this->trailer,true);
		$criteria->compare('createdDate',$this->createdDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
