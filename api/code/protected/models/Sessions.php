<?php

/**
 * This is the model class for table "sessions".
 *
 * The followings are the available columns in table 'sessions':
 * @property string $id
 * @property string $itemId
 * @property string $stateRoomId
 * @property integer $seatsAvailable
 * @property string $description
 * @property string $startTime
 * @property string $expireTime
 * @property string $createdDate
 * @property string $price
 * @property string $status
 * @property string $userId
 * @property string $access
 *
 * The followings are the available model relations:
 * @property Reservation[] $reservations
 * @property Items $item
 * @property Stateroom $stateRoom
 * @property User $user
 */
class Sessions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sessions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('itemId, stateRoomId', 'required'),
			array('seatsAvailable', 'numerical', 'integerOnly'=>true),
			array('itemId, stateRoomId, userId', 'length', 'max'=>10),
			array('price', 'length', 'max'=>45),
			array('status', 'length', 'max'=>9),
			array('access', 'length', 'max'=>7),
			array('description, startTime, expireTime, createdDate', 'safe'),
                        array('createdDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, itemId, stateRoomId, seatsAvailable, description, startTime, expireTime, createdDate, price, status, userId, access', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'reservations' => array(self::HAS_MANY, 'Reservation', 'sessionId'), /*This table must not visible to user*/
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
			'stateRoom' => array(self::BELONGS_TO, 'Stateroom', 'stateRoomId'),
			'venue'=>array(self::BELONGS_TO,'Venue',array('venueId'=>'id'),'through'=>'stateRoom'),
			'itemCat'=>array(self::BELONGS_TO,'Itemcategories',array('catId'=>'id'),'through'=>'item'),
			//'venueCat'=>array(self::BELONGS_TO,'Venuecategories',array('catId'=>'id'),'through'=>'venue'),
                        //'rating' => array(self::HAS_MANY, 'Rating', array('id'=>'postId'), 'through'=>'item', 'condition'=>'rating.type=\'item\''),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'itemId' => 'Item',
			'stateRoomId' => 'State Room',
			'seatsAvailable' => 'Seats Available',
			'description' => 'Description',
			'startTime' => 'Start Time',
			'expireTime' => 'Expire Time',
			'createdDate' => 'Created Date',
			'price' => 'Price',
			'status' => 'Status',
			'userId' => 'User',
			'access' => 'Access',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('stateRoomId',$this->stateRoomId,true);
		$criteria->compare('seatsAvailable',$this->seatsAvailable);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('startTime',$this->startTime,true);
		$criteria->compare('expireTime',$this->expireTime,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('access',$this->access,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sessions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
