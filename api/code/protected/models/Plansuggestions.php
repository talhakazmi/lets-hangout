<?php

/**
 * This is the model class for table "plansuggestions".
 *
 * The followings are the available columns in table 'plansuggestions':
 * @property string $id
 * @property string $planId
 * @property string $sessionsId
 * @property string $userId
 * @property string $createdDate
 * @property integer $isActive
 *
 * The followings are the available model relations:
 * @property Plan $plan
 * @property Sessions $sessions
 * @property User $user
 * @property Plansuggestionvote[] $plansuggestionvotes
 */
class Plansuggestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'plansuggestions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('planId, sessionsId, userId', 'required'),
			array('isActive', 'numerical', 'integerOnly'=>true),
			array('planId, sessionsId, userId', 'length', 'max'=>10),
			array('createdDate', 'safe'),
                        array('createdDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, planId, sessionsId, userId, createdDate, isActive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'plan' => array(self::BELONGS_TO, 'Plan', 'planId'),
			'sessions' => array(self::BELONGS_TO, 'Sessions', 'sessionsId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'plansuggestionvotes' => array(self::HAS_MANY, 'Plansuggestionvote', 'planSuggestionsId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'planId' => 'Plan',
			'sessionsId' => 'Sessions',
			'userId' => 'User',
			'createdDate' => 'Created Date',
			'isActive' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('planId',$this->planId,true);
		$criteria->compare('sessionsId',$this->sessionsId,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('isActive',$this->isActive);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Plansuggestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
