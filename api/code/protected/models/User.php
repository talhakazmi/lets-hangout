<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $createdDate
 * @property integer $isActive
 * @property string $code
 *
 * The followings are the available model relations:
 * @property Circle[] $circles
 * @property Comments[] $comments
 * @property Friends[] $friends
 * @property Friends[] $friends1
 * @property Notifications[] $notifications
 * @property Notifications[] $notifications1
 * @property Plan[] $plans
 * @property Plansuggestions[] $plansuggestions
 * @property Plansuggestionvote[] $plansuggestionvotes
 * @property Rating[] $ratings
 * @property Reservation[] $reservations
 * @property Sessions[] $sessions
 * @property Venue[] $venues
 */
class User extends CActiveRecord
{       
        public $repeat_password = null;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
			array('email', 'unique', 'className' => 'User', 'attributeName' => 'email', 'message'=>'This Email is already in use'),
                        array('isActive', 'numerical', 'integerOnly'=>true),
			array('email, password, name', 'length', 'max'=>45),
			array('code, createdDate', 'safe'),
                        array('createdDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			array('repeat_password', 'required', 'on'=>'insert'),
                        array('password', 'compare', 'compareAttribute'=>'repeat_password', 'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, name, createdDate, isActive, code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'circles' => array(self::HAS_MANY, 'Circle', 'userId'),
			'comments' => array(self::HAS_MANY, 'Comments', 'userId'),
			'friends' => array(self::HAS_MANY, 'Friends', 'userId'),
			//'friends1' => array(self::HAS_MANY, 'Friends', 'friendId'),
			'notifications' => array(self::HAS_MANY, 'Notifications', 'userId', 'condition'=>'isActive=1'),
			//'notifications1' => array(self::HAS_MANY, 'Notifications', 'senderId'),
			'plans' => array(self::HAS_MANY, 'Plan', 'userId'),
			'plansuggestions' => array(self::HAS_MANY, 'Plansuggestions', 'userId'),
			'plansuggestionvotes' => array(self::HAS_MANY, 'Plansuggestionvote', 'userId'),
			'ratings' => array(self::HAS_MANY, 'Rating', 'userId'),
			'reservations' => array(self::HAS_MANY, 'Reservation', 'userId'),
			'sessions' => array(self::HAS_MANY, 'Sessions', 'userId'),
			'venues' => array(self::HAS_MANY, 'Venue', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'name' => 'Name',
			'createdDate' => 'Created Date',
			'isActive' => 'Is Active',
			'code' => 'Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('isActive',$this->isActive);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
