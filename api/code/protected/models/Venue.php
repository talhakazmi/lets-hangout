<?php

/**
 * This is the model class for table "venue".
 *
 * The followings are the available columns in table 'venue':
 * @property string $id
 * @property string $catId
 * @property string $title
 * @property string $country
 * @property string $city
 * @property string $address
 * @property integer $latitude
 * @property integer $longitude
 * @property string $createdDate
 * @property string $userId
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Stateroom[] $staterooms
 * @property User $user
 * @property Venuecategories $cat
 */
class Venue extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'venue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('catId, title, address', 'required'),
			array('latitude, longitude', 'numerical', 'integerOnly'=>false),
			array('catId, userId', 'length', 'max'=>10),
                        array('status', 'in', 'range'=>array('public','private'), 'allowEmpty'=>false),
			array('title, country, city, address', 'length', 'max'=>255),
                        array('createdDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, catId, title, country, city, address, latitude, longitude, createdDate, userId, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'staterooms' => array(self::HAS_MANY, 'Stateroom', 'venueId'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'cat' => array(self::BELONGS_TO, 'Venuecategories', 'catId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'catId' => 'Cat',
			'title' => 'Title',
			'country' => 'Country',
			'city' => 'City',
			'address' => 'Address',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'createdDate' => 'Created Date',
			'userId' => 'User',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('catId',$this->catId,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Venue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
