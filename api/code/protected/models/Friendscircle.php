<?php

/**
 * This is the model class for table "friendscircle".
 *
 * The followings are the available columns in table 'friendscircle':
 * @property string $id
 * @property string $circleId
 * @property string $friendsId
 *
 * The followings are the available model relations:
 * @property Circle $circle
 * @property Friends $friends
 */
class Friendscircle extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'friendscircle';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('circleId, friendsId', 'required'),
			array('circleId, friendsId', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, circleId, friendsId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'circle' => array(self::BELONGS_TO, 'Circle', 'circleId'),
			'friends' => array(self::BELONGS_TO, 'Friends', 'friendsId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'circleId' => 'Circle',
			'friendsId' => 'Friends',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('circleId',$this->circleId,true);
		$criteria->compare('friendsId',$this->friendsId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Friendscircle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
