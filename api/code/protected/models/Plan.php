<?php

/**
 * This is the model class for table "plan".
 *
 * The followings are the available columns in table 'plan':
 * @property string $id
 * @property string $title
 * @property string $status
 * @property string $userId
 * @property string $createdDate
 * @property string $updatedDate
 * @property integer $isSuggestion
 * @property integer $isInvite
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Planinvites[] $planinvites
 * @property Plansuggestions[] $plansuggestions
 */
class Plan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'plan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId', 'required'),
			array('isSuggestion, isInvite', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			array('status', 'length', 'max'=>9),
			array('userId', 'length', 'max'=>10),
			array('createdDate, updatedDate', 'safe'),
                        array('createdDate,updatedDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('updatedDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, status, userId, createdDate, updatedDate, isSuggestion, isInvite', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
			'planinvites' => array(self::HAS_MANY, 'Planinvites', 'planId'),
			'plansuggestions' => array(self::HAS_MANY, 'Plansuggestions', 'planId'),
            'friends' => array(self::BELONGS_TO, 'Friends', array('friendsId'=>'id'), 'through'=>'planinvites'),
            'sessions' => array(self::HAS_MANY, 'Sessions', array('sessionsId'=>'id'), 'through'=>'plansuggestions'),
            'stateroom' => array(self::HAS_MANY, 'Stateroom', array('stateRoomId'=>'id'), 'through'=>'sessions'),
            'venue' => array(self::HAS_MANY, 'Venue', array('venueId'=>'id'), 'through'=>'stateroom'),
            'items' => array(self::HAS_MANY, 'Items', array('itemId'=>'id'), 'through'=>'sessions'),
            'votes' => array(self::HAS_MANY, 'Plansuggestionvote', array('id'=>'planSuggestionsId'), 'through'=>'plansuggestions'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'status' => 'Status',
			'userId' => 'User',
			'createdDate' => 'Created Date',
			'updatedDate' => 'Updated Date',
			'isSuggestion' => 'Is Suggestion',
			'isInvite' => 'Is Invite',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('updatedDate',$this->updatedDate,true);
		$criteria->compare('isSuggestion',$this->isSuggestion);
		$criteria->compare('isInvite',$this->isInvite);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Plan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
