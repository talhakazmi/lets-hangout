<?php

/**
 * This is the model class for table "stateroom".
 *
 * The followings are the available columns in table 'stateroom':
 * @property string $id
 * @property string $title
 * @property string $venueId
 * @property integer $totalSeats
 * @property string $description
 * @property string $createdDate
 * @property string $updatedDate
 *
 * The followings are the available model relations:
 * @property Sessions[] $sessions
 * @property Venue $venue
 */
class Stateroom extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stateroom';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, venueId', 'required'),
			array('totalSeats', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			array('venueId', 'length', 'max'=>10),
			array('description, updatedDate', 'safe'),
                        array('createdDate,updatedDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
                        array('updatedDate','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, venueId, totalSeats, description, createdDate, updatedDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sessions' => array(self::HAS_MANY, 'Sessions', 'stateRoomId'),
			'venue' => array(self::BELONGS_TO, 'Venue', 'venueId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'venueId' => 'Venue',
			'totalSeats' => 'Total Seats',
			'description' => 'Description',
			'createdDate' => 'Created Date',
			'updatedDate' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('venueId',$this->venueId,true);
		$criteria->compare('totalSeats',$this->totalSeats);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('updatedDate',$this->updatedDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stateroom the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
