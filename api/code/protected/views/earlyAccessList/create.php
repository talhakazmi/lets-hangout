<?php
/* @var $this EarlyAccessListController */
/* @var $model EarlyAccessList */

$this->breadcrumbs=array(
	'Early Access Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EarlyAccessList', 'url'=>array('index')),
	array('label'=>'Manage EarlyAccessList', 'url'=>array('admin')),
);
?>

<h1>Create EarlyAccessList</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>