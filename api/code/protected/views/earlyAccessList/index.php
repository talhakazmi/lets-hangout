<?php
/* @var $this EarlyAccessListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Early Access Lists',
);

$this->menu=array(
	array('label'=>'Create EarlyAccessList', 'url'=>array('create')),
	array('label'=>'Manage EarlyAccessList', 'url'=>array('admin')),
);
?>

<h1>Early Access Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
