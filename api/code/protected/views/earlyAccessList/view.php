<?php
/* @var $this EarlyAccessListController */
/* @var $model EarlyAccessList */

$this->breadcrumbs=array(
	'Early Access Lists'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EarlyAccessList', 'url'=>array('index')),
	array('label'=>'Create EarlyAccessList', 'url'=>array('create')),
	array('label'=>'Update EarlyAccessList', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EarlyAccessList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EarlyAccessList', 'url'=>array('admin')),
);
?>

<h1>View EarlyAccessList #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'createdDate',
	),
)); ?>
