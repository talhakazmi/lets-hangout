<?php
/* @var $this EarlyAccessListController */
/* @var $model EarlyAccessList */

$this->breadcrumbs=array(
	'Early Access Lists'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EarlyAccessList', 'url'=>array('index')),
	array('label'=>'Create EarlyAccessList', 'url'=>array('create')),
	array('label'=>'View EarlyAccessList', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EarlyAccessList', 'url'=>array('admin')),
);
?>

<h1>Update EarlyAccessList <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>