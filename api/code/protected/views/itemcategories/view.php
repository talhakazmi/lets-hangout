<?php
/* @var $this ItemcategoriesController */
/* @var $model Itemcategories */

$this->breadcrumbs=array(
	'Itemcategories'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Itemcategories', 'url'=>array('index')),
	array('label'=>'Create Itemcategories', 'url'=>array('create')),
	array('label'=>'Update Itemcategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Itemcategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Itemcategories', 'url'=>array('admin')),
);
?>

<h1>View Itemcategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'logo',
		'venueCatId',
		'createdDate',
	),
)); ?>
