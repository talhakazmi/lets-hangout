<?php
/* @var $this ItemcategoriesController */
/* @var $model Itemcategories */

$this->breadcrumbs=array(
	'Itemcategories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Itemcategories', 'url'=>array('index')),
	array('label'=>'Manage Itemcategories', 'url'=>array('admin')),
);
?>

<h1>Create Itemcategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>