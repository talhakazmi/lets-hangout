<?php
/* @var $this ItemcategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Itemcategories',
);

$this->menu=array(
	array('label'=>'Create Itemcategories', 'url'=>array('create')),
	array('label'=>'Manage Itemcategories', 'url'=>array('admin')),
);
?>

<h1>Itemcategories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
