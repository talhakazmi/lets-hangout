<?php
/* @var $this ItemcategoriesController */
/* @var $model Itemcategories */

$this->breadcrumbs=array(
	'Itemcategories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Itemcategories', 'url'=>array('index')),
	array('label'=>'Create Itemcategories', 'url'=>array('create')),
	array('label'=>'View Itemcategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Itemcategories', 'url'=>array('admin')),
);
?>

<h1>Update Itemcategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>