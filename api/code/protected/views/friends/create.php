<?php
/* @var $this FriendsController */
/* @var $model Friends */

$this->breadcrumbs=array(
	'Friends'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Friends', 'url'=>array('index')),
	array('label'=>'Manage Friends', 'url'=>array('admin')),
);
?>

<h1>Create Friends</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>