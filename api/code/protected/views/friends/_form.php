<?php
/* @var $this FriendsController */
/* @var $model Friends */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'friends-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'User'); ?>
                <?php echo $form->dropDownList($model,'userId',CHtml::listData(User::model()->findAll(), 'id', 'email'));?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Friend'); ?>
                <?php echo $form->dropDownList($model,'friendId',CHtml::listData(User::model()->findAll(), 'id', 'email'));?>
	</div>
	<div class="row">
		<?php echo $form->dropDownList($model,'status',array('pending'=>'Pending Request','approved'=>'Approve request','decline'=>'Reject Friendship')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->