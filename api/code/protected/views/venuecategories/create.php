<?php
/* @var $this VenuecategoriesController */
/* @var $model Venuecategories */

$this->breadcrumbs=array(
	'Venuecategories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Venuecategories', 'url'=>array('index')),
	array('label'=>'Manage Venuecategories', 'url'=>array('admin')),
);
?>

<h1>Create Venuecategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>