<?php
/* @var $this VenuecategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Venuecategories',
);

$this->menu=array(
	array('label'=>'Create Venuecategories', 'url'=>array('create')),
	array('label'=>'Manage Venuecategories', 'url'=>array('admin')),
);
?>

<h1>Venuecategories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
