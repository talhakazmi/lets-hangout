<?php
/* @var $this VenuecategoriesController */
/* @var $model Venuecategories */

$this->breadcrumbs=array(
	'Venuecategories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Venuecategories', 'url'=>array('index')),
	array('label'=>'Create Venuecategories', 'url'=>array('create')),
	array('label'=>'View Venuecategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Venuecategories', 'url'=>array('admin')),
);
?>

<h1>Update Venuecategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>