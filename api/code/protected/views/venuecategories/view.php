<?php
/* @var $this VenuecategoriesController */
/* @var $model Venuecategories */

$this->breadcrumbs=array(
	'Venuecategories'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Venuecategories', 'url'=>array('index')),
	array('label'=>'Create Venuecategories', 'url'=>array('create')),
	array('label'=>'Update Venuecategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Venuecategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Venuecategories', 'url'=>array('admin')),
);
?>

<h1>View Venuecategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'createdDate',
	),
)); ?>
