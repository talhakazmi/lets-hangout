<?php
/* @var $this PlansuggestionvoteController */
/* @var $model Plansuggestionvote */

$this->breadcrumbs=array(
	'Plansuggestionvotes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Plansuggestionvote', 'url'=>array('index')),
	array('label'=>'Create Plansuggestionvote', 'url'=>array('create')),
	array('label'=>'View Plansuggestionvote', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Plansuggestionvote', 'url'=>array('admin')),
);
?>

<h1>Update Plansuggestionvote <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>