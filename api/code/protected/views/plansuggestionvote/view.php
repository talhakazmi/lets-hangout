<?php
/* @var $this PlansuggestionvoteController */
/* @var $model Plansuggestionvote */

$this->breadcrumbs=array(
	'Plansuggestionvotes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Plansuggestionvote', 'url'=>array('index')),
	array('label'=>'Create Plansuggestionvote', 'url'=>array('create')),
	array('label'=>'Update Plansuggestionvote', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Plansuggestionvote', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Plansuggestionvote', 'url'=>array('admin')),
);
?>

<h1>View Plansuggestionvote #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'planSuggestionsId',
		'userId',
		'vote',
		'createdDate',
	),
)); ?>
