<?php
/* @var $this PlansuggestionvoteController */
/* @var $model Plansuggestionvote */

$this->breadcrumbs=array(
	'Plansuggestionvotes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Plansuggestionvote', 'url'=>array('index')),
	array('label'=>'Manage Plansuggestionvote', 'url'=>array('admin')),
);
?>

<h1>Create Plansuggestionvote</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>