<?php
/* @var $this PlansuggestionvoteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plansuggestionvotes',
);

$this->menu=array(
	array('label'=>'Create Plansuggestionvote', 'url'=>array('create')),
	array('label'=>'Manage Plansuggestionvote', 'url'=>array('admin')),
);
?>

<h1>Plansuggestionvotes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
