<?php
/* @var $this CommentsController */
/* @var $model Comments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comments-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'type',array('venue'=>'Venue', 'item'=>'Item')); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'postId'); ?>
		<?php echo $form->textField($model,'postId',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'postId'); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'status',array('pending'=>'Pending', 'approved'=>'Approved', 'discarded'=>'Discarded')); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'userId',CHtml::listData(User::model()->findAll(), 'id', 'email'));?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->