<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>Let's Hangout</title>
</head>
<body>

<table cellpadding="0" cellspacing="0" border="0" style="width:100%; height:100%; background:#f3f3f3;">

<tr>
    <td align="center">

        <!-- Email Body -->
        <table cellpadding="0" cellspacing="0" border="0" style="width:600px; ">
            <tr>
                <td style="background:#1e81d6; height:74px; padding-left:15px;">
                    <img src="<?php echo Yii::app()->params['frontEndServer'] ?>assets/img/logo.png" alt="The Blimp"/>
                </td>
            </tr>
            <tr>
                <td style="padding:50px; background:#fff; border:1px solid #ddd">
                    <?php echo $content; ?>
                </td>
            </tr>
            <tr>
            <td align="center">
                <p style="font-family:arial; color:#666; font-size:11px; padding:10px;"></p>
            </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</body>
</html>