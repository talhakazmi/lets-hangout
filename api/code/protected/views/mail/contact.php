<p style="font-family:arial; color:#666; font-size:12px;">
    <h1>Welcome <?php echo $name ?>,</h1>
    <p><?php echo $message ?></p>
</p>
<br />
<a href="<?php echo $link ?>" target="_blank" style="font-family:arial;background-color:#1383e1; border:1px solid #0264b6; border-radius:4px; padding:10px 18px; color:#fff; font-size:14px; font-weight:bold; text-align:center; text-decoration:none;"><?php echo $label ?></a>
<br>
<p>
</p>
If the button isn't working, click the link below or copy and paste it on your browser.
<p>
    <br>
    <a href="<?php echo $link ?>" target="_blank"><?php echo $link ?></a>
    <br><br>    
    From, <br>
    <?php echo $sender ?>
</p>