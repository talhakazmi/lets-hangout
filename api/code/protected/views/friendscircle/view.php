<?php
/* @var $this FriendscircleController */
/* @var $model Friendscircle */

$this->breadcrumbs=array(
	'Friendscircles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Friendscircle', 'url'=>array('index')),
	array('label'=>'Create Friendscircle', 'url'=>array('create')),
	array('label'=>'Update Friendscircle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Friendscircle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Friendscircle', 'url'=>array('admin')),
);
?>

<h1>View Friendscircle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'circleId',
		'friendsId',
	),
)); ?>
