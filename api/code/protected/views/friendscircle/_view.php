<?php
/* @var $this FriendscircleController */
/* @var $data Friendscircle */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('circleId')); ?>:</b>
	<?php echo CHtml::encode($data->circleId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('friendsId')); ?>:</b>
	<?php echo CHtml::encode($data->friendsId); ?>
	<br />


</div>