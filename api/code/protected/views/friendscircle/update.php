<?php
/* @var $this FriendscircleController */
/* @var $model Friendscircle */

$this->breadcrumbs=array(
	'Friendscircles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Friendscircle', 'url'=>array('index')),
	array('label'=>'Create Friendscircle', 'url'=>array('create')),
	array('label'=>'View Friendscircle', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Friendscircle', 'url'=>array('admin')),
);
?>

<h1>Update Friendscircle <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>