<?php
/* @var $this FriendscircleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Friendscircles',
);

$this->menu=array(
	array('label'=>'Create Friendscircle', 'url'=>array('create')),
	array('label'=>'Manage Friendscircle', 'url'=>array('admin')),
);
?>

<h1>Friendscircles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
