<?php
/* @var $this FriendscircleController */
/* @var $model Friendscircle */

$this->breadcrumbs=array(
	'Friendscircles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Friendscircle', 'url'=>array('index')),
	array('label'=>'Manage Friendscircle', 'url'=>array('admin')),
);
?>

<h1>Create Friendscircle</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>