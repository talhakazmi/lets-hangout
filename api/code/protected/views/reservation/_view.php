<?php
/* @var $this ReservationController */
/* @var $data Reservation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sessionId')); ?>:</b>
	<?php echo CHtml::encode($data->sessionId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seatsReserved')); ?>:</b>
	<?php echo CHtml::encode($data->seatsReserved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seatsPosition')); ?>:</b>
	<?php echo CHtml::encode($data->seatsPosition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cost')); ?>:</b>
	<?php echo CHtml::encode($data->cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bookingAgentId')); ?>:</b>
	<?php echo CHtml::encode($data->bookingAgentId); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('createdDate')); ?>:</b>
	<?php echo CHtml::encode($data->createdDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	*/ ?>

</div>