<?php
/* @var $this ReservationController */
/* @var $model Reservation */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reservation-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->dropDownList($model,'sessionId',CHtml::listData(Sessions::model()->findAll(), 'id', 'id'));?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'seatsReserved'); ?>
		<?php echo $form->textField($model,'seatsReserved'); ?>
		<?php echo $form->error($model,'seatsReserved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'seatsPosition'); ?>
		<?php echo $form->textField($model,'seatsPosition',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'seatsPosition'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cost'); ?>
		<?php echo $form->textField($model,'cost'); ?>
		<?php echo $form->error($model,'cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'status',array('active'=>'Active','pending'=>'Pending','onhold'=>'On Hold','cancelled'=>'Cancelled')); ?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'bookingAgentId',CHtml::listData(Bookingagent::model()->findAll(), 'id', 'name'));?>
	</div>

	<div class="row">
		<?php echo $form->dropDownList($model,'userId',CHtml::listData(User::model()->findAll(), 'id', 'email'));?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->