<?php
/* @var $this CircleController */
/* @var $model Circle */

$this->breadcrumbs=array(
	'Circles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Circle', 'url'=>array('index')),
	array('label'=>'Create Circle', 'url'=>array('create')),
	array('label'=>'View Circle', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Circle', 'url'=>array('admin')),
);
?>

<h1>Update Circle <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>