<?php
/* @var $this BookingagentController */
/* @var $model Bookingagent */

$this->breadcrumbs=array(
	'Bookingagents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bookingagent', 'url'=>array('index')),
	array('label'=>'Manage Bookingagent', 'url'=>array('admin')),
);
?>

<h1>Create Bookingagent</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>