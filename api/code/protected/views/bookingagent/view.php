<?php
/* @var $this BookingagentController */
/* @var $model Bookingagent */

$this->breadcrumbs=array(
	'Bookingagents'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Bookingagent', 'url'=>array('index')),
	array('label'=>'Create Bookingagent', 'url'=>array('create')),
	array('label'=>'Update Bookingagent', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Bookingagent', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bookingagent', 'url'=>array('admin')),
);
?>

<h1>View Bookingagent #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'email',
		'designation',
		'status',
		'supervisorEmail',
		'createdDate',
		'updatedDate',
	),
)); ?>
