<?php
/* @var $this BookingagentController */
/* @var $model Bookingagent */

$this->breadcrumbs=array(
	'Bookingagents'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bookingagent', 'url'=>array('index')),
	array('label'=>'Create Bookingagent', 'url'=>array('create')),
	array('label'=>'View Bookingagent', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Bookingagent', 'url'=>array('admin')),
);
?>

<h1>Update Bookingagent <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>