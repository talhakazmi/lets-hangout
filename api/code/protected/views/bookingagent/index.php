<?php
/* @var $this BookingagentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bookingagents',
);

$this->menu=array(
	array('label'=>'Create Bookingagent', 'url'=>array('create')),
	array('label'=>'Manage Bookingagent', 'url'=>array('admin')),
);
?>

<h1>Bookingagents</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
