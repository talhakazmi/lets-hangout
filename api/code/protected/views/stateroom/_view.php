<?php
/* @var $this StateroomController */
/* @var $data Stateroom */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('venueId')); ?>:</b>
	<?php echo CHtml::encode($data->venueId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalSeats')); ?>:</b>
	<?php echo CHtml::encode($data->totalSeats); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdDate')); ?>:</b>
	<?php echo CHtml::encode($data->createdDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updatedDate')); ?>:</b>
	<?php echo CHtml::encode($data->updatedDate); ?>
	<br />


</div>