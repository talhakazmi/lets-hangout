<?php
/* @var $this StateroomController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Staterooms',
);

$this->menu=array(
	array('label'=>'Create Stateroom', 'url'=>array('create')),
	array('label'=>'Manage Stateroom', 'url'=>array('admin')),
);
?>

<h1>Staterooms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
