<?php
/* @var $this StateroomController */
/* @var $model Stateroom */

$this->breadcrumbs=array(
	'Staterooms'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Stateroom', 'url'=>array('index')),
	array('label'=>'Create Stateroom', 'url'=>array('create')),
	array('label'=>'View Stateroom', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Stateroom', 'url'=>array('admin')),
);
?>

<h1>Update Stateroom <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>