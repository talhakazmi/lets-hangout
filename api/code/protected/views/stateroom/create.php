<?php
/* @var $this StateroomController */
/* @var $model Stateroom */

$this->breadcrumbs=array(
	'Staterooms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Stateroom', 'url'=>array('index')),
	array('label'=>'Manage Stateroom', 'url'=>array('admin')),
);
?>

<h1>Create Stateroom</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>