<?php
/* @var $this PlansuggestionsController */
/* @var $model Plansuggestions */

$this->breadcrumbs=array(
	'Plansuggestions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Plansuggestions', 'url'=>array('index')),
	array('label'=>'Create Plansuggestions', 'url'=>array('create')),
	array('label'=>'View Plansuggestions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Plansuggestions', 'url'=>array('admin')),
);
?>

<h1>Update Plansuggestions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>