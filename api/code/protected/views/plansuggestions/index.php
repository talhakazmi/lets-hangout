<?php
/* @var $this PlansuggestionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Plansuggestions',
);

$this->menu=array(
	array('label'=>'Create Plansuggestions', 'url'=>array('create')),
	array('label'=>'Manage Plansuggestions', 'url'=>array('admin')),
);
?>

<h1>Plansuggestions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
