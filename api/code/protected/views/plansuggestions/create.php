<?php
/* @var $this PlansuggestionsController */
/* @var $model Plansuggestions */

$this->breadcrumbs=array(
	'Plansuggestions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Plansuggestions', 'url'=>array('index')),
	array('label'=>'Manage Plansuggestions', 'url'=>array('admin')),
);
?>

<h1>Create Plansuggestions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>