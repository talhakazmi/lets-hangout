<?php
/* @var $this PlansuggestionsController */
/* @var $model Plansuggestions */

$this->breadcrumbs=array(
	'Plansuggestions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Plansuggestions', 'url'=>array('index')),
	array('label'=>'Create Plansuggestions', 'url'=>array('create')),
	array('label'=>'Update Plansuggestions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Plansuggestions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Plansuggestions', 'url'=>array('admin')),
);
?>

<h1>View Plansuggestions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'planId',
		'sessionsId',
		'userId',
		'createdDate',
		'isActive',
	),
)); ?>
