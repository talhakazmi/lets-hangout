<?php
/* @var $this PlaninvitesController */
/* @var $model Planinvites */

$this->breadcrumbs=array(
	'Planinvites'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Planinvites', 'url'=>array('index')),
	array('label'=>'Manage Planinvites', 'url'=>array('admin')),
);
?>

<h1>Create Planinvites</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>