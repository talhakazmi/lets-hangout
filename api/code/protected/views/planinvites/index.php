<?php
/* @var $this PlaninvitesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Planinvites',
);

$this->menu=array(
	array('label'=>'Create Planinvites', 'url'=>array('create')),
	array('label'=>'Manage Planinvites', 'url'=>array('admin')),
);
?>

<h1>Planinvites</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
