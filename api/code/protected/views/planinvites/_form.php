<?php
/* @var $this PlaninvitesController */
/* @var $model Planinvites */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'planinvites-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'planId'); ?>
		<?php echo $form->dropDownList($model,'planId',CHtml::listData(Plan::model()->findAll(), 'id', 'title'));?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'friendsId'); ?>
		<?php echo $form->dropDownList($model,'friendsId',CHtml::listData(User::model()->findAll(), 'id', 'email'));?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'initialStatus'); ?>
		<?php echo $form->dropDownList($model,'initialStatus',array('yes'=>'Yes, I\'m coming','maybe'=>'Not sure','no'=>'No')); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'finalStatus'); ?>
		<?php echo $form->dropDownList($model,'finalStatus',array('departed'=>'On my way','late'=>'I\'ll be late','cancel'=>'Droping out','arrived'=>'I have reached')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->