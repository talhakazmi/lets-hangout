<?php
/* @var $this PlaninvitesController */
/* @var $model Planinvites */

$this->breadcrumbs=array(
	'Planinvites'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Planinvites', 'url'=>array('index')),
	array('label'=>'Create Planinvites', 'url'=>array('create')),
	array('label'=>'Update Planinvites', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Planinvites', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Planinvites', 'url'=>array('admin')),
);
?>

<h1>View Planinvites #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'planId',
		'friendsId',
		'initialStatus',
		'finalStatus',
		'createdDate',
	),
)); ?>
