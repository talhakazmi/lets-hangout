<?php
/* @var $this PlaninvitesController */
/* @var $data Planinvites */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('planId')); ?>:</b>
	<?php echo CHtml::encode($data->planId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('friendsId')); ?>:</b>
	<?php echo CHtml::encode($data->friendsId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('initialStatus')); ?>:</b>
	<?php echo CHtml::encode($data->initialStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('finalStatus')); ?>:</b>
	<?php echo CHtml::encode($data->finalStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdDate')); ?>:</b>
	<?php echo CHtml::encode($data->createdDate); ?>
	<br />


</div>