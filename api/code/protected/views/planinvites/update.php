<?php
/* @var $this PlaninvitesController */
/* @var $model Planinvites */

$this->breadcrumbs=array(
	'Planinvites'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Planinvites', 'url'=>array('index')),
	array('label'=>'Create Planinvites', 'url'=>array('create')),
	array('label'=>'View Planinvites', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Planinvites', 'url'=>array('admin')),
);
?>

<h1>Update Planinvites <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>