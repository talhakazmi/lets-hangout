First install Docker

run following command on root folder to up container and get application working

    docker-compose up --build

run following commands to install openssl and generate ssl certificates on container ot get https url for facebook login

     docker exec -it testing /bin/bash

     mkdir /etc/nginx/ssl

     openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt

     service nginx restart

Now application will run on https just proceed with unsafe browsing.